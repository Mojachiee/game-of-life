#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

enum {gridSize = 10};


struct grid{
  char col[gridSize][gridSize];
  int size;
};
typedef struct grid grid;

void readline(int n, char line[n]) {
    fflush(stdout);
    fgets(line, n, stdin);
    line[strcspn(line, "\r\n")] = '\0';
}

void readColony(grid *grid) {
    for (int i=0; ! feof(stdin); i++)
    {
      readline(gridSize, grid->col[i]);
      if (feof(stdin))
      {
        strcpy(grid->col[i], "");
        grid->size = i;
      }
    }
}

void printColony(grid *grid)
{
  for (int i=0; i < grid->size; i++)
  {
    for (int n=0; n < grid->size; n++)
    {
      printf("%c", grid->col[i][n]);
    }
    printf("\n");
  }
}

int getNeighbourCount(grid *grid, int x, int y)
{
  int count = 0;
  for (int i=(x-1); i <(x+2); i++)
  {
    for (int n=(y-1); n<(y+2); n++)
    {
      if (i ==-1 && n==-1)
      {
          if (grid->col[grid->size - 1][grid->size - 1] == '#')
          {
            count ++;
          }
      } else if (i==-1) {
        if (grid->col[grid->size - 1][n] == '#')
        {
          count ++;
        }
      } else if (n==-1) {
        if (grid->col[i][grid->size - 1] == '#')
        {
          count ++;
        }
      } else if (i == grid->size + 1 && n == grid->size + 1) {
        if (grid->col[0][0] == '#')
        {
          count ++;
        }
      } else if (i == grid->size +1) {
        if (grid->col[0][n] == '#')
        {
          count ++;
        }
      } else if (n == grid->size +1) {
        if (grid->col[i][0] == '#')
        {
          count ++;
        }
      } else if (grid->col[i][n] == '#' && !(x==i && y==n)) {
        count ++;
      }
    }
  }
  return count;
}

void createNextTick(grid *grid)
{
  char newCol[gridSize][gridSize];
  for (int i = 0; i < grid->size; i++ )
  {
    for (int n = 0; n <grid->size; n++)
    {
      int neighbours = getNeighbourCount(grid,i,n);
      if (grid->col[i][n] == '#')
      {
        if (neighbours == 2 || neighbours == 3)
        {
          newCol[i][n] = '#';
        } else {
          newCol[i][n] = '.';
        }
      } else {
        if (neighbours == 3)
        {
          newCol[i][n] = '#';
        } else {
          newCol[i][n] = '.';
        }
      }
    }
  }
  memcpy(*(grid->col),&newCol,sizeof(grid->col));
}

int main(int n, char *args[n])
{
  grid *grid = malloc(sizeof(grid));
  int tickCount = 1;
  if (n > 1)
  {
    tickCount= atoi(args[1]);
  }
  for (int i = 0; i < tickCount; i++)
  {
    readColony(grid);
    createNextTick(grid);
    printColony(grid);
  }
}
